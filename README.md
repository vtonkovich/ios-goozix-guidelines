# iOS Goozix Guidelines for Swift

This document describes the main rules which it's necessary to follow for keeping code and other parts of project readable and clear.

## Official Swift Guidelines

*  These [design guidelines](https://swift.org/documentation/api-design-guidelines/) explain how to make sure that your code feels like a part of the larger Swift ecosystem.
*  There is a [language guide](https://docs.swift.org/swift-book/LanguageGuide/TheBasics.html)
*  This [part](https://docs.swift.org/swift-book/ReferenceManual/AboutTheLanguageReference.html) describes the formal grammar of the Swift programming language. 

## Unofficial Swift Style Guideline

Each project should use this [style guideline](https://github.com/raywenderlich/swift-style-guide) from raywenderlich.com.

## Code Organization

For keeping code readable should use `#MARK` to categorize methods in functional groupings and protocol/delegate implementations following this general structure.

```
#MARK: - Lifecycle
#MARK: - Init
#MARK: - IBActions
#MARK: - Public
#MARK: - Private
#MARK: - Protocol conformance
```


Each block of any class which conformances protocol/delegate should use extensions for better readability:

```Swift
// MARK: - TransactionPresenter

extension TransactionPresenterImpl: TransactionPresenter {
  // something
}
```

## Formatting Tool

Each project should use this [library](https://github.com/nicklockwood/SwiftFormat) for reformatting Swift code.

## Git Flow

### Branches

The repository should have two main branches: `develop` and `master`. `Master` branch contains the current application version from the store. `Develop` is the main branch for development, it is used as a base for every new branch. When the development and testing of a new version are finished, the code should be merged from `develop` to `master`. Every branch should solve only a single task.

There are only two types of branches: `hotfix` and `feature`. Bug prefix should be used for bugs logic. `Feature` prefix should be used for new logic/functionality of the application. This two types of branches should be created from `develop` and merged into it via PullRequest.

### Pull Requests

When the development and testing are finished, pull request should be merged in develop, and the temp branch should be removed.
If there is only one developer on the project, pull request is used only for merging and testing branch separately from any changes of the other tasks, else it is also used for the code review. 
When the development of the logic is finished, the developer should create a pull request from a temp branch to develop.

### Commits desctiption

#### Rules for a great git commit message style:

*  Capitalize the message line.
*  End the message line with a period
*  Wrap lines at 72 characters
*  Write your commit message in the indicative: "Fixed bug" and not "Fix bug" or "Fixes bug."
*  Start the commit message with [+] if you add new features or files, [-] if you remove features or files, [*] if you modify features or files. Separate commit message from [+], [-], [*] with space. Use symbol [+], [*], [-] one time per commit message.
*  Use the body to explain what and why you have done something. In most cases, you can leave out details about how a change has been made.

#### Information in commit messages

* Describe why a change is being made.
* How does it address the issue?
* What effects does the patch have?
* Do not assume the reviewer understands what the original problem was.
* Do not assume the code is self-evident/self-documenting. 
* The first commit line is the most important.
* Describe any limitations of the current code.

### Rewriting History

Before sending pull request it's necessary to rewrite the history of the previous commits with all excess information in a particular branch. You can use `git rebase` command.

This [information](https://git-scm.com/book/en/v2/Git-Tools-Rewriting-History) would be enough for understanding how to do it.